#! python

import os
import subprocess
import tkinter
from git import Repo
from tkinter import *
from tkinter import ttk
from subprocess import PIPE
from tkinter.filedialog import askdirectory

choice_text = ''
result_line = ()
master = 'master'
develop = 'develop'
trade = 'trade'
short_name_checkout = ''
file_git = '/.git'
filename = ''

def message_error(text_er, text_er1):
    root = Tk()
    root.title("Error")
    frame = Frame(root)
    message = Message(frame, text="При работе фукции " + text_er + " произошла ошибка" + text_er1, width=300)
    message.config(fg='red', font=('times', 11))
    frame.pack()
    message.pack()
    root.minsize(200, 100)
    root.mainloop()

def select_close():
    try:
        os.system('git checkout master')
        sys.exit()
    except Exception:
        message_error('закрытия программы(select_close)', '!!!')

def get_askdir():
    global filename
    global file_git
    clone = 'git clone https://bitbucket.org/******'
    pull = 'git pull'
    init = 'git submodule init'
    upd_subm = 'git submodule update'
    try:
        def restart():
            try:
                tkinter.Tk().deiconify()
                root.quit()
                root.destroy()
                sys.stdout.flush()
                os.execl(sys.executable, os.path.abspath(__file__), *sys.argv)
            except Exception:
                message_error('Перезагрузки программы', '!!!')

        # запуск меню выбора файла
        tkinter.Tk().withdraw()
        filename = askdirectory(title='Необходимо выбрать каталог с проектом(в котором находятся файл .git(он может быть скрытым))')
        if not os.path.exists(filename + file_git):
            def get_create_clon():
                global filename

                def subp(funk, x):
                    try:
                        subprocess.call(funk, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
                        pb['value'] = x
                        pb.update()
                    except Exception:
                        message_error('Клонирования в каталог', '!!!')
                try:
                    os.chdir(filename)
                except Exception:
                    message_error("Выбора каталога", "!!!")
                subp(clone, 25)
                filename += '/cuwi'
                os.chdir(filename)
                subp(init, 50)
                subp(pull, 75)
                subp(upd_subm, 100)
                root.quit()
                root.destroy()

            root = Tk()
            root.title("Ошибка")
            root.geometry("400x250")
            lab = Label(root, text="В выбранном каталоге не обнаружен файл .git.\nRestart-Повторить выбор\n"
                                   "Cancel-Закрыть программу\nClone-Клонировать проект в выбранный каталог", width=50,
                        height=10, font=('times', 12))
            lab.place(relx=.5, rely=.3, anchor="c")
            btn_clone = Button(root, text="Clone", command=get_create_clon, width=6, height=2, font=('times', 12))
            btn_clone.place(relx=.5, rely=.8, anchor="c")
            btn_restart = Button(root, text="Restart", command=restart, width=6, height=2, font=('times', 12))
            btn_restart.place(relx=.2, rely=.8, anchor="c")
            btn_cancel = Button(root, text="Cancel", command=select_close, width=6, height=2, font=('times', 12))
            btn_cancel.place(relx=.8, rely=.8, anchor="c")
            pb = ttk.Progressbar(root, value=0, orient="horizontal", length=300, mode="determinate")
            pb.place(relx=0.5, rely=0.6, anchor="c", bordermode="inside")
            root.mainloop()
    except Exception:
        message_error('Вызова окна ошибки', '!!!')

get_askdir()

GIT_REPO_PATH = filename

def print_commit(commit):
    try:
        global result_line
        x = 0
        oneline_hesha = str(commit.hexsha)
        oneline = ("\"{}\" by {} ({})".format(commit.summary, commit.author.name, commit.author.email))
        date = str(commit.authored_datetime)
        result_line = list(result_line)
        result_line.insert(x, oneline_hesha + ' ---->> ' + oneline + ' <<---- ' + date)
        result_line = tuple(result_line)
        x += 1
    except Exception:
        message_error('опрос гита по комитам(print_commit)', '!!!')

def print_repository(repo):
    try:
        print('Repo description: {}'.format(repo.description))
        print('Repo active branch is {}'.format(repo.active_branch))
        for remote in repo.remotes:
            print('Remote named "{}" with URL "{}"'.format(remote, remote.url))
        print('Last commit for repo is {}.'.format(str(repo.head.commit.hexsha)))
    except Exception:
        message_error('опрос гита по комитам(print_repository)', '!!!')

def get_checkout():
    try:
        global short_name_checkout
        reslist = list()
        seleccion = listbox.curselection()
        for i in seleccion:
            entrada = listbox.get(i)
            reslist.append(entrada)
            full_name_checkout = (str(entrada).split(' ')[0])
        short_name_checkout = (str(full_name_checkout)[0:11])
        os.system('git checkout ' + short_name_checkout)
        os.system('git submodule update')
        label_name_check = Label(frame, text=('Now commit:  ' + short_name_checkout), width=20, height=1, font='times 11')
        label_name_check.grid(column=10, row=1)
        label_name_check.config(bg='#e7f236')
        print(short_name_checkout)
    except Exception:
        message_error('перехода по веткам(get_checkout), нужно выбрать строку из списка для перехода, ', '!!!')

def get_start(name):
    if __name__ == "__main__":
        try:
            global repo_path
            global repo
            repo_path = os.getenv(filename)
            # Repo object used to programmatically interact with Git repositories
            repo = Repo(repo_path)
            # check that the repository loaded correctly
            if not repo.bare:
                print('Repo at {} successfully loaded.'.format(repo_path))
                print_repository(repo)
                # create list of commits then print some of them to stdout
                commits = list(repo.iter_commits(name))
                for commit in commits:
                    print_commit(commit)
                    pass
            else:
                print('Could not load repository at {} :('.format(repo_path))
        except Exception:
            message_error(', запуска программы(get_start) проверьте выбранный каталог проекта,', '!!!')

os.system('git checkout master')
get_start(master)

root = Tk()
root.title("Choice to Commit")
root.wm_geometry("%dx%d+%d+%d" % (1200, 600, 0, 0))
frame = ttk.Frame(root, padding=(7, 7, 11, 11))
frame.grid(column=0, row=0, sticky=(N, S, E, W))
listbox_items = list(result_line)

def select_item():
    try:
        reslist = list()
        seleccion = listbox.curselection()
        for i in seleccion:
            entrada = listbox.get(i)
            reslist.append(entrada)
        for val in reslist:
            info_label = Tk()
            info_label.title("Info for Commit")
            info = ttk.Frame(info_label, padding=(7, 7, 11, 11))
            info.grid(column=0, row=0, sticky=(N, S, E, W))
            l1 = Label(info, text=val, font='times 12', wraplength=800, justify=LEFT)
            l1.config(bd=10)
            l1.place(relx=10, rely=10)
            l1.pack()
    except Exception:
        message_error('отображения информации о комите(select_item)', '!!!')

def get_clear_listbox():
    try:
        global result_line
        global listbox
        result_line = ''
        listbox.delete(first=0, last=END)
    except Exception:
        message_error('очистки листа(clear_listbox)', '!!!')

def get_check_trade():
    try:
        os.chdir(filename)
        get_clear_listbox()
        os.system('git checkout trade')
        os.system('git submodule update')
        color = '#36f255'
        get_start(trade)
        listbox_items = list(result_line)
        for item in listbox_items:
            if item.split(' ')[0] == str(repo.head.commit.hexsha):
                listbox.config(bg='White', fg='Black')
                listbox.insert(0, item)
            else:
                listbox.config(bg='Black', fg='White')
                listbox.insert(0, item)
                continue
        label_status = Label(frame, text=repo.active_branch, width=5, height=1, font='times 11')
        label_status.grid(column=1, row=1)
        label_status.config(bg=color, bd=4)
        label_name_check = Label(frame, text=('Now commit:  ' + str(repo.head.commit.hexsha)[0:11]), width=20, height=1, font='times 11')
        label_name_check.grid(column=10, row=1)
        label_name_check.config(bg=color)
    except Exception:
        message_error('перехода в ветку trade(check_trade)', '!!!')

def get_check_master():
    try:
        os.chdir(filename)
        get_clear_listbox()
        os.system('git checkout master')
        os.system('git submodule update')
        color = '#ffaaaa'
        get_start(master)
        listbox_items = list(result_line)
        for item in listbox_items:
            listbox.insert(0, item)
        label_status = Label(frame, text=repo.active_branch, width=5, height=1, font='times 11')
        label_status.grid(column=1, row=1)
        label_status.config(bg=color, bd=4)
        label_name_check = Label(frame, text=('Now commit:  ' + str(repo.head.commit.hexsha)[0:11]), width=20, height=1, font='times 11')
        label_name_check.grid(column=10, row=1)
        label_name_check.config(bg=color)
    except Exception:
        message_error('перехода в ветку master(check_master)', '!!!')

def get_check_develop():
    try:
        os.chdir(filename)
        get_clear_listbox()
        os.system('git checkout develop')
        os.system('git submodule update')
        color = '#0de4ea'
        get_start(develop)
        listbox_items = list(result_line)
        for item in listbox_items:
            listbox.insert(0, item)
        label_status = Label(frame, text=repo.active_branch, width=5, height=1, font='times 11')
        label_status.grid(column=1, row=1)
        label_status.config(bg=color, bd=4)
        label_name_check = Label(frame, text=('Now commit:  ' + str(repo.head.commit.hexsha)[0:11]), width=20, height=1, font='times 11')
        label_name_check.grid(column=10, row=1)
        label_name_check.config(bg=color)
    except Exception:
        message_error('перехода в ветку develop(check_develop)', '!!!')

label_status = Label(frame, text=repo.active_branch, font='times 11')
label_status.grid(column=1, row=1)
label_status.config(bg='#e7f236')
label_name_check = Label(frame, text=('Now commit:  ' + str(repo.head.commit.hexsha)[0:11]), width=20, height=1, font='times 11')
label_name_check.grid(column=10, row=1)
label_name_check.config(bg='#e7f236')
btn_info = ttk.Button(frame, text="Info", command=select_item)
btn_info.grid(column=2, row=1)
btn_info_label = ttk.Button(frame, text="Git Checkout/Submodule update", command=get_checkout)
btn_info_label.grid(column=3, row=1)
btn_develop = ttk.Button(frame, text="Checkout Develop", command=get_check_develop)
btn_develop.grid(column=4, row=1)
btn_trade = ttk.Button(frame, text="Checkout Trade", command=get_check_trade)
btn_trade.grid(column=5, row=1)
btn_master = ttk.Button(frame, text='Checkout Master', command=get_check_master)
btn_master.grid(column=6, row=1)
btn_close = ttk.Button(frame, text='Exit', command=select_close)
btn_close.grid(column=7, row=1)
listbox = Listbox(root, width=140, height=25, font=('times', 12), selectbackground='BLUE')
listbox.place(x=4, y=33)

for item in listbox_items:
    listbox.insert(0, item)

root.mainloop()